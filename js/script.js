$(document).ready(function() {
    $('#load-content').click(function(e) {
        // prevent the links default action
        // from firing
        e.preventDefault();

        // attempt to GET the new content
        $.get('content', function(data) {
            $('#content').html(data);
        });
    })
});