$.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    addDragable();
    addHovered();
    hoverEdit(".hover-edit",".taskTitle");
    hoverDelete(".hover-delete",".addNewPerson");
    addAutoCompleteTo("members", "memberList");
    addAutoCompleteTo("team", "teammember");
    $(".fixed-action-btn").openFAB();
    $(".fixed-action-btn").closeFAB();
    $('.tooltipped').tooltip({delay: 50});
    $( "#deadline" ).datepicker({
        dateFormat: "dd-mm-yy"
    });

    $( "#deadlineEdit" ).datepicker({
        dateFormat: "dd-mm-yy"
    });
    $( "#deadline").datepicker( "option", "monthNames", [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ] );
});
var rowCount = $("#mainfocus > tr").length;
var newChecklistToBesent = [];
var i = 0;
var $person, $task, $cell;
var idTaskToBeSent;
var idPersonToBeSent;
var idTaskToEdit;
var idPersonToEdit;
var cellToEdit;
var split;
var oldTask;
var projectN;
function getColor(progress){
    switch (progress){
        case -1:{
            return "red lighten-2 ";
        }break;
        case 0:{
            return "orange lighten-2 ";
        }break;
        case 1:{
            return "green lighten-2 ";
        }break;
    }
}
try {
    subtask.forEach(function (entry) {
        cellID = "" + entry.task_id + "-" + entry.to_whom;
        var card="<div class=\"card-panel "+ getColor(parseInt(entry.status) )+"\"" + "id=\"" + entry.id + "\" "+ " onclick=\"subtask.edit(this, this.parentNode)\"><p class=\"subtaskcontent\" data-toggle=\"modal\" data-target=\"#editsubtask\">" + entry.subtask_name +"</p><a href=\"#\"><i class=\"tiny material-icons\" onclick=\"subtask.remove(this.parentNode.parentNode)\">delete</i></a></div>";
        document.getElementById(cellID).innerHTML += (card);
        print(entry.status);
    });
} catch (err) {

}
subtask = "";
var checklist = 0;
var util = (function () {
    return {
        replaceAll: function (string, find, replace) {
            return string.replace(new RegExp(find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), 'g'), replace);
        }
    }
})();
function tilt_direction(item) {
    var left_pos = item.position().left,
        move_handler = function (e) {
            if (e.pageX >= left_pos) {
                item.addClass("right");
                item.removeClass("left");
            } else {
                item.addClass("left");
                item.removeClass("right");
            }
            left_pos = e.pageX;
        };
    $("html").bind("mousemove", move_handler);
    item.data("move_handler", move_handler);
}


function addDragable() {
    $(".portlet").sortable({
        items: '.card-panel',
        placeholder: "portlet-placeholder",
        /*cursor: 'crosshair',*/
        connectWith: '.portlet',
        dropOnEmpty: true,
        receive: function (e, ui) {
            $(this).append(ui.item);
            split = ui.item.parent().attr('id').split('-');
            console.log("after: " + split + ', subtask' + ui.item.attr('id'));
            $.post(URL + "/subtask/" + userID,
                {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    project_id: projectID,
                    task_id: split[0],
                    person_id: split[1],
                    old_task_id: oldTask,
                    subtask_id: ui.item.attr('id')
                },
                function (data, status) {
                    $("#dump").html(data);
                    info(data);
                });
        },
        start: function (event, ui) {
            ui.placeholder.height(ui.item.height());
            ui.item.addClass('tilt');
            tilt_direction(ui.item);
            oldTask = ui.item.parent().closest('tr').find('th').attr('id');
            oldTask = oldTask.substring(5, oldTask.length);
            //$("#addSubtask").remove();
            console.log("before: task " + oldTask + ', subtask ' + ui.item.attr('id'));
        },
        stop: function (event, ui) {
            ui.item.removeClass("tilt");
            $("html").unbind('mousemove', ui.item.data("move_handler"));
            ui.item.removeData("move_handler");

        }
    });
}
function addHovered() {
    /*$('td').hover(
     function () {
     $(this).find("button").remove();
     $(this).prepend($("<button type=\"button\"  data-toggle=\"modal\" data-target=\"#addsubtask\" id=\"addSubtask\" onclick=\"addSubtask(this.parentNode)\"><i class=\"medium material-icons\">note_add</i></button>"));

     }, function () {
     $(this).find("button").remove();
     }
     );*/
}

function info(data) {
    if (data === "error") {
        toastr.error("Debug: Failed");
        return false;
    } else if (data === "redirect") {
        toastr.error("Debug: This project is private");
        return false;
    } else if (data === "login") {
        toastr.error("Debug: Login first");
        return false;
    }
    else {
        toastr.success("Debug: Success");
        return true;
    }
}
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
function print(msg) {
    console.log(msg);
}
//todo add person confirm
function hoverDelete(nodeAppend, root) {
    $(root).hover(
        function () {
            $(this).find(nodeAppend).find("button").remove();
            $(this).find(nodeAppend).append($("<button type=\"button\" style=\"margin-left: 10px\" onclick=\"deleteThis(this.parentNode.parentNode)\">delete</button>"));
        }, function () {
            $(this).find(nodeAppend).find("button:last").remove();
        }
    );
}
function hoverEdit(nodeAppend, root) {

    $(root).hover(
        function () {
            $(this).find(nodeAppend).find("button").remove();
            $(this).find(nodeAppend).append($("<button type=\"button\" style=\"margin-left: 10px\"  data-toggle=\"modal\" data-target=\"#edittask\" onclick=\"deleteThis(this.parentNode.parentNode)\" >Edit</button>"));
        }, function () {
            $(this).find(nodeAppend).find("button:last").remove();
        }
    );
}
//todo hhhhh
function deleteThis(element) {
    if (element.id.indexOf("task") > -1) {
        //$("#" + element.id).closest("tr").remove();
        var judul=$("#"+element.id).find("#judultask").text();
        var dueDate=$("#"+element.id).find("#duedate").text();
        print(judul+" "+dueDate);
        $("#deadlineEdit").val(dueDate);
        $("#edittaskname").val(judul);
    } else {
        var raw = element.id;
        var personID = raw.substring(7, raw.length);
        if (personID == owner) {
            toastr.warning("Failed, delete project instead");
        }
         $.post(URL + "/project/" + projectID+"/inproject/"+personID,
         {
         _method: "delete"
         },
         function (data, status) {
         if (info(data)) {
             $("#" + raw).remove();
             print(personID);
             $("#mainfocus").find('tr').each(function () {
                 try {
                     var raw = $(this).find('th').attr('id');
                     var taskID = raw.substring(5, raw.length);
                     $('#' + taskID + '-' + personID).remove();
                 } catch (err) {

                 }

             });
         } else {
         //info(data);
         }
         });
    }

}
function recordExistedPerson() {
    existedPersonID = [];
    index = 0;
    $("#persontitle").find('th').each(function () {
        if (index > 0) {
            var value = $(this).attr('id');
            existedPersonID.push(parseInt(value.substring(7, value.length)));
            console.log(value.substring(7, value.length));
        }
        index++;
    });
}
var index = 0;
var existedPersonID = [];
function addAutoCompleteTo(element, element2) {
    $("#" + element).autocomplete({
        source: "/user",
        minLength: 2,
        select: function (event, ui) {
            print("sdfgsdfg");
            var url = parseInt(ui.item.id);
            var label = ui.item.label;
            if (url != '#') {
                if (memberArr.length === 0) {
                    if ($.inArray(url, existedPersonID) === -1) {
                        memberArr.push(url);
                        memberArrname.push(label);

                        $("#" + element2).append("<div class=\"chip\" id=\"" + url + "\">" + label + "<i class=\"material-icons\" onclick=\"person.remove(this.parentNode)\">close</i></div>");
                    }
                } else if ($.inArray(url, memberArr) === -1) {
                    if ($.inArray(url, existedPersonID) === -1) {
                        memberArr.push(url);
                        memberArrname.push(label);
                        $("#" + element2).append("<div class=\"chip\" id=\"" + url + "\">" + label + "<i class=\"material-icons\" onclick=\"person.remove(this.parentNode)\">close</i></div>");
                    }
                }
            }
        }, html: true,
        open: function (event, ui) {
            $(".ui-autocomplete").css("z-index", 1050);
        },
        close: function (event, ui) {
            $("#members").val("");
            $("#team").val("");
        }
    });
}
$(function () {
    $("#members");
});

var memberArr = [];
var memberArrname = [];
var checkList = 0;
var pCheckList = 0;
$("#addnewchecklist").click(function () {
    if ($("#namechecklist").val() === "") {
        return;
    }
    $("#checklist").append("<p id=\"p" + pCheckList + "\"><input type=\"checkbox\" value=\"" + $("#namechecklist").val() + "\" name=\"just-checkbox-to-add\" class=\"filled-in\" id=\"" + "checklistNew" + checkList + "\" onclick=\"checkboxListener(this.id)\"/><label for=\"" + "checklistNew" + checkList + "\">" + $("#namechecklist").val() + "</label></p>");
    $("#namechecklist").val("");
    checkboxListener("checklistNew" + checkList);
    checkList++;
    hoverDelete($(this).find("p"));
});
$("#newchecklistinedit").click(function () {
    if ($("#editnamechecklist").val() === "") {
        return;
    }
    $("#checklisttoedit").append("<p id=\"p" + pCheckList + "\"><input type=\"checkbox\" value=\"" + $("#editnamechecklist").val() + "\" name=\"checkbox-to-edit\" class=\"filled-in\" id=\"" + "checklistExisting" + checklist + "\" onclick=\"checkboxListener(this.id)\"/><label for=\"" + "checklistExisting" + checklist + "\">" + $("#editnamechecklist").val() + "</label></p>");
    $("#editnamechecklist").val("");
    checkboxListener("checklistExisting" + checklist);
    checklist++;
    hoverDelete($(this).find("p"));
    checkboxCounter("null");
});
function checkboxCounter(element) {
    var checkedCount = 0, uncheckedCount = 0;
    var total = 0;
    var totalInPercentage = 0;
    if (element.indexOf("checklistNew") > -1) {
        print("Add new subtask " + checkList);
        $('input:checkbox[name=just-checkbox-to-add]:checked').each(function () {
            checkedCount++;
        });
        $("input:checkbox[name=just-checkbox-to-add]:not(:checked)").each(function () {
            uncheckedCount++;
        });
        total = checkedCount + uncheckedCount;
        totalInPercentage = (checkedCount / total) * 100;
        $('#addsubtaskprogress').width(totalInPercentage + '%');
    } else {
        print("edit existing subtask " + checklist);
        $('input:checkbox[name=checkbox-to-edit]:checked').each(function () {
            checkedCount++;
        });
        $("input:checkbox[name=checkbox-to-edit]:not(:checked)").each(function () {
            uncheckedCount++;

        });

        total = checkedCount + uncheckedCount;
        totalInPercentage = (checkedCount / total) * 100;
        $('#editsubtaskprogress').width(totalInPercentage + '%');
    }
    print(total + " " + totalInPercentage);
}
function checkboxListener(element) {
    $("#" + element).change(function () {
        checkboxCounter(element);
    });
}

var subtask = (function () {
    var add = function (element1) {
            $("#checklist").empty();
            $("#namechecklist").val("");
            $("#namecard").val("");
            checkList = 0;
            console.log("id " + element1.id);
            var split = element1.id.split('-');
            $("#addCardTitle").html("Add card to person " + split[1] + " in task " + split[0]);
            idTaskToBeSent = split[0];
            idPersonToBeSent = split[1];
            $cell = element1.id;
        },
        edit = function (element, element2) {
            checklist = 0;
            cellToEdit = element.id;
            var split = element2.id.split('-');
            $("#addCardTitle").html("Edit card to person " + split[1] + " in task " + split[0]);
            idTaskToEdit = split[0];
            idPersonToEdit = split[1];
            $("#checklisttoedit").empty();
            print(element.id + "   ke 2 " + element2.id);
            $.get(URL + "/subtask/" + element.id, function (data, status) {
                print(data);
                for (var i = 0; i < data.length; i++) {
                    var obj = data[i];
                    for (var key in obj) {
                        var attrName = key;
                        var attrValue = obj[key];
                        if (attrName === "subtask_name") {
                            $("#editnamecard").val(attrValue);
                        }
                        if (attrName === "status") {
                            attrValue = parseInt(attrValue);
                            switch (attrValue) {
                                case -1:
                                {
                                    $("#status-1").prop("checked", true)
                                }
                                    break;
                                case 0:
                                {
                                    $("#status0").prop("checked", true)
                                }
                                    break;
                                case 1:
                                {
                                    $("#status1").prop("checked", true)
                                }
                                    break;
                            }
                        }
                        if (attrName === "checked" && attrValue != "") {
                            if (attrValue.indexOf(',') === -1) {
                                attrValue = util.replaceAll(attrValue, "[skipday]", ",");
                                $("#checklisttoedit").append("<p><input type=\"checkbox\" checked=\"checked\" value=\"" + attrValue + "\" name=\"checkbox-to-edit\" class=\"filled-in\" id=\"" + "checklistExisting" + checklist + "\"/><label for=\"" + "checklistExisting" + checklist + "\">" + attrValue + "</label></p>");
                                checkboxListener("checklistExisting" + checklist);
                                checklist++;
                            } else {
                                attrValue = attrValue.split(',');
                                for (var j in attrValue) {
                                    attrValue[j] = util.replaceAll(attrValue[j], "[skipday]", ",");
                                    $("#checklisttoedit").append("<p><input type=\"checkbox\" checked=\"checked\" value=\"" + attrValue[j] + "\" name=\"checkbox-to-edit\" class=\"filled-in\" id=\"" + "checklistExisting" + checklist + "\"/><label for=\"" + "checklistExisting" + checklist + "\">" + attrValue[j] + "</label></p>");
                                    checkboxListener("checklistExisting" + checklist);
                                    checklist++;
                                }
                            }
                        } else if (attrName === "unchecked" && attrValue != "") {
                            if (attrValue.indexOf(',') === -1) {
                                attrValue = util.replaceAll(attrValue, "[skipday]", ",");
                                $("#checklisttoedit").append("<p><input type=\"checkbox\" value=\"" + attrValue + "\" name=\"checkbox-to-edit\" class=\"filled-in\" id=\"" + "checklistExisting" + checklist + "\"/><label for=\"" + "checklistExisting" + checklist + "\">" + attrValue + "</label></p>");
                                checkboxListener("checklistExisting" + checklist);
                                checklist++;
                            } else {
                                attrValue = attrValue.split(',');
                                for (var k in attrValue) {
                                    attrValue[k] = util.replaceAll(attrValue[k], "[skipday]", ",");
                                    $("#checklisttoedit").append("<p><input type=\"checkbox\" value=\"" + attrValue[k] + "\" name=\"checkbox-to-edit\" class=\"filled-in\" id=\"" + "checklistExisting" + checklist + "\"/><label for=\"" + "checklistExisting" + checklist + "\">" + attrValue[k] + "</label></p>");
                                    checkboxListener("checklistExisting" + checklist);
                                    checklist++;
                                }
                            }
                        }
                    }
                }
                checkboxCounter("null");
            });
        },
        remove = function (element) {
            var subtaskID = element.id;
            print("eeeeee "+subtaskID);
        },
        adding = function () {
            var checked = [];
            var unchecked = [];
            if ($("#namecard").val() === "") {
                toastr.warning("Insert card name");
                return;
            }
            $('input:checkbox[name=just-checkbox-to-add]:checked').each(function () {
                var value = $(this).val();
                if (value.indexOf(',') === -1) {
                    print("no comma");
                } else {
                    print("comma");
                    value = value.replace(/,/g, "[skipday]");
                    print(value);
                }
                checked.push(value);
            });

            $("input:checkbox[name=just-checkbox-to-add]:not(:checked)").each(function () {
                var value = $(this).val();
                if (value.indexOf(',') === -1) {
                    print("no comma");
                } else {
                    print("comma");
                    value = value.replace(/,/g, "[skipday]");
                    print(value);
                }
                unchecked.push(value);
            });
            var namecard = $("#namecard").val();//todo is here
            var card="<div id=\"temp_card\" class=\"card-panel red lighten-2\" onclick=\"subtask.edit(this, this.parentNode)\"><p class=\"subtaskcontent\" data-toggle=\"modal\" data-target=\"#editsubtask\">" + namecard + "</p><a href=\"#\"><i class=\"tiny material-icons\" onclick=\"subtask.remove(this.parentNode.parentNode)\">delete</i></a></div>";
            $('#' + $cell).append(card);
            $.post(URL + "/subtask",
                {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    project_id: projectID,
                    task_id: idTaskToBeSent,
                    person_id: idPersonToBeSent,
                    subtask_name: namecard,
                    checked: checked.toString(),
                    unchecked: unchecked.toString(),
                    user_id: userID
                },
                function (data, status) {
                    print(data);
                    if (info(data)) {
                        print("this id is " + data);
                        $('#temp_card').attr('id', data);
                    } else {
                        $('#' + $cell).find("#temp_card").remove();
                    }
                });

            $('#addsubtask').modal('hide');
        },
        editing = function () {
            var subtaskStatus = -1;
            $('input[name=card-status]:checked').each(function () {
                var rawID = $(this).attr('id');
                subtaskStatus = rawID.substring(6, rawID.length);
            });
            var checked = [];
            var unchecked = [];
            if ($("#editnamecard").val() === "") {
                return;
            }
            $('input:checkbox[name=checkbox-to-edit]:checked').each(function () {
                var value = $(this).val();
                if (value.indexOf(',') === -1) {
                    print("no comma");
                } else {
                    print("comma");
                    value = value.replace(/,/g, "[skipday]");
                    print(value);
                }
                checked.push(value);
            });

            $("input:checkbox[name=checkbox-to-edit]:not(:checked)").each(function () {
                var value = $(this).val();
                if (value.indexOf(',') === -1) {
                    print("no comma");
                } else {
                    print("comma");
                    value = value.replace(/,/g, "[skipday]");
                    print(value);
                }
                unchecked.push(value);
            });

            $.post(URL + "/subtask/editsubtask",
                {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    project_id: projectID,
                    task_id: idTaskToEdit,
                    subtask_id: cellToEdit,
                    subtask_status: subtaskStatus,
                    subtask_name: $("#editnamecard").val(),
                    checked: checked.toString(),
                    unchecked: unchecked.toString(),
                    user_id: userID
                },//todo asdfasdfsdf
                function (data, status) {
                    if (info(data)) {
                        $("td").find("#"+cellToEdit).attr('class', "card-panel "+ getColor(parseInt(subtaskStatus)));
                        print($('td').find("#" + cellToEdit).text());
                        $('td').find("#" + cellToEdit).find('p').text($("#editnamecard").val());
                    }

                });
            $('#editsubtask').modal('hide');
        };
    return {
        add: function (param) {
            add(param);
        },
        edit: function (param, param2) {
            edit(param, param2);
        },
        remove: function (param) {
            remove(param);
        },
        adding: function () {
            adding();
        },
        editing: function () {
            editing();
        }
    }
})();

var task = (function () {
    var add = function () {
            $("#taskname").val("");
            $("#taskdescription").val("");
            recordExistedPerson();
        },
        edit = function (element, element2) {
        },
        remove = function (element) {
        },
        adding = function () {
            if ($("#taskname").val() === "" || $("#deadline").val() === "") {
                print("clicked");
                return;
            }
            $.post(URL + "/task",
                {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    project_id: projectID,
                    task_name: $("#taskname").val(),
                    task_desc: $("#taskdescription").val(),
                    due_date: $("#deadline").val(),
                    user_id: userID
                },
                function (data, status) {
                    if (info(data)) {
                        $("#dump").html(data.id);
                        $("#mainfocus").append("<tr><th id=\"task_" + data + "\"  class=\"taskTitle\"><div id=\"judultask\"><p>" + $("#taskname").val() + "</p></div><div id=\"duedate\"><p>" + $("#deadline").val() + "</p></div><div id=\"deskripsi\" style=\"display:none\"><p>" + $("#taskdescription").val() + "</p></div><div class=\"hover-edit\"><div></th>");
                        hoverEdit(".hover-edit",".taskTitle");
                        recordExistedPerson();//todo
                        for (var i = 0; i < existedPersonID.length; i++) {
                            $("tbody tr:last-child").append("<td id=\"" + data + "-" + existedPersonID[i] + "\" class=\"portlet  ui-sortable\"><div class=\"spacer\"><button type=\"button\" class=\"btn btn-smxxx \" data-toggle=\"modal\" data-target=\"#addsubtask\" id=\"addSubtask\" onclick=\"subtask.add(this.parentNode.parentNode)\">+</button></div></td>");
                            addDragable();
                            addHovered();
                        }
                    }
                });
        },
        editing = function () {

            var checked = [];
            var unchecked = [];
            if ($("#editnamecard").val() === "") {
                return;
            }
            $('input:checkbox[name=checkbox-to-edit]:checked').each(function () {
                var value = $(this).val();
                if (value.indexOf(',') === -1) {
                    print("no comma");
                } else {
                    print("comma");
                    value = value.replace(/,/g, "[skipday]");
                    print(value);
                }
                checked.push(value);
            });

            $("input:checkbox[name=checkbox-to-edit]:not(:checked)").each(function () {
                var value = $(this).val();
                if (value.indexOf(',') === -1) {
                    print("no comma");
                } else {
                    print("comma");
                    value = value.replace(/,/g, "[skipday]");
                    print(value);
                }
                unchecked.push(value);
            });

            $.post(URL + "/subtask/editsubtask",
                {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    project_id: projectID,
                    task_id: idTaskToEdit,
                    subtask_id: cellToEdit,
                    subtask_name: $("#editnamecard").val(),
                    checked: checked.toString(),
                    unchecked: unchecked.toString(),
                    user_id: userID
                },
                function (data, status) {
                    if (info(data)) {
                        print($('td').find("#" + cellToEdit).text());
                        $('td').find("#" + cellToEdit).find('p').text($("#editnamecard").val());
                    }

                });
            $('#editsubtask').modal('hide');
        };
    return {
        add: function () {
            add();
        },
        edit: function (param, param2) {
            edit(param, param2);
        },
        remove: function (param) {
            remove(param);
        },
        adding: function () {
            adding();
        },
        editing: function () {
            editing();
        }
    }
})();

var project = (function () {
    var add = function () {
            print("click");
            memberArr = [];
            memberArrname = [];
            existedPersonID = [];
            existedPersonID.push(userID);
            $("#projectName").val("");
            $("#projectDetail").val("");
            $("#teammember").empty();
        },
        adding = function (projectName, projectDescription, icon) {
            var detail = $("#projectDetail").val();
            var name = $("#projectName").val();
            var card = "<div class=\"col s12 m12 l4\" id=\"project_temp\">" +
                    "<div id=\"profile-card\" class=\"card\">"+
                "<div class=\"card-image waves-effect waves-block waves-light\" id=\"project_temp2\">" +
                "<?php $d=rand(1,1)?>" +
                "<a href=\"#\"> <img src=\"" + URL + "/images/kawaii/1.jpg" + "\">" +
                "</a>" +
                "<span class=\"card-title\">" + name + "</span>" +
                "</div>" +
                "<div class=\"card-content\">" +
                "<a href=\"#\" class=\"close\"  id=\"deleteProject\"><i class=\"small material-icons\">delete</i></a>" +
                "<a href=\"#\" class=\"close\" id=\"editProject\"><i class=\"small material-icons\">mode_edit</i></a>" +
                "</div>" +
                "</div>" +
                "</div>";
            $(".row").append(card);
            projectN = projectName;
            print("Sent " + memberArr);
            $.post(URL + "/project",
                {
                    project_name: name,
                    project_description: detail,
                    project_icon: icon,
                    project_owner: userID,
                    project_team_member: memberArr
                },
                function (data, status) {
                    if (info(data)) {
                        $("#project_temp2").find("img").attr("src", URL + "/images/kawaii/" + rand(1, 1) + ".jpg");
                        $("#project_temp2").find("a").attr("href", URL + "/project/" + data);
                        $('#project_temp2').attr('id', data);
                        $('#project_temp').attr('id', data);
                        //$(".row").append("<h5><a href=\"" + URL +"/project/" +data + "\">" + $("#projectName").val() + "</a></h5>");
                    } else {
                        $('#project_temp').remove();
                    }

                    print(URL + data);
                    //window.location.href = URL + "/project/" + data;
                });
        },
        edit = function () {
        },
        editing = function () {
        },
        remove = function () {
            var rawID = $("#deleteProject").parentsUntil("#profile-card").parent().parent().attr('id');
            print(rawID);
            var projectID = rawID.substring(8, rawID.length);
            $.post(URL + "/project/" + projectID,
                {
                    _method: "delete"
                },
                function (data, status) {
                    if (info(data)) {
                        $('#project_' + projectID).remove();
                    } else {
                        //info(data);
                    }
                });
        };
    return {
        add: function () {
            add();
        },
        adding: function (param, param2, param3) {
            adding(param, param2, param3);
        },
        edit: function () {
            edit();
        },
        editing: function () {
            editing();
        },
        remove: function () {
            remove();
        }
    }
})();

var person = (function () {
    var add = function () {
            memberArr = [];
            memberArrname = [];
            recordExistedPerson();
            $("#members").val("");
            $("#memberList").empty();
        },
        adding = function () {
            var i = 0;
            var personIDTemp = 0;
            var task = [];
            for (var h in memberArr) {
                task = [];
                i = 0;
                $("#mainfocus").find('tr').each(function () {
                    if (i === 0) {
                        $("#persontitle").append("<th class=\"addNewPerson\" id=\"person_" + memberArr[h] + "\"> <p style=\"width: 110px;text-align: center;\">" + memberArrname[h] + "</p><div class=\"hover-delete\"><div></th>");
                        hoverDelete(".hover-delete",".addNewPerson");
                    } else {
                        var taskID = $(this).find('th').attr('id');
                        $(this).append("<td id=\"" + taskID.substring(5, taskID.length) + "-" + memberArr[h] + "\" class=\"portlet\">" + "<div class=\"spacer\"><button type=\"button\" class=\"btn btn-smxxx \" data-toggle=\"modal\" data-target=\"#addsubtask\" id=\"addSubtask\" onclick=\"subtask.add(this.parentNode.parentNode)\">+</button></div></td>");
                        task.push(taskID.substring(5, taskID.length));
                        addDragable();
                        addHovered();
                    }
                    i++;
                });
                personIDTemp++;
            }
            $.post(URL + "/project/" + projectID+"/inproject",
                {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    member_list: memberArr
                },
                function (data, status) {
                    print("^^^^^^^^^^^"+projectID);
                    info(data);
                    print("Kembali " + data);

                });
        },
        edit = function () {
        },
        editing = function () {
        },
        remove = function (element) {
            var id = parseInt(element.id);
            var index = $.inArray(id, memberArr);
            print(element.id + " " + memberArr + " " + index);
            if (index > -1) {
                memberArrname.splice(index, 1);
            }
            memberArr = $.grep(memberArr, function (value) {
                return value != element.id;
            });
        };
    return {
        add: function () {
            add();
        },
        adding: function () {
            adding();
        },
        edit: function () {
            edit();
        },
        editing: function () {
            editing();
        },
        remove: function (element) {
            remove(element);
        }
    }
})();