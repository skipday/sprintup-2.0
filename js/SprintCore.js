/**
 * Created by jagamypriera on 16/11/15.
 */
var MAIN = "main";
var WELCOME = "welcome";
var PROJECT = "project-screen";
var userName = "", uid, email;
var first = true;
var reg = $('#signup_fragment');
var app = angular.module("sprintup", ["firebase"]);
var rootRef = new Firebase('https://blazing-heat-5124.firebaseio.com/');
var projects = rootRef.child('projects');
var project = (function (id) {
    return projects.child(id);
});
var tasks = (function (pID) {
    return projects.child(pID).child('task');
});
var task = (function (pID, tID) {
    return projects.child(pID).child('task').child(tID);
});
function toggleReveal() {
    $('#regPassword').attr('type', $('#regPassword').attr('type') === 'password' ? 'text' : 'password');
}
function buka(task, user) {
    $('*[data-id="tempo"]').attr('id', task + "/" + user);
    $('#modalSubTask').openModal();
}
function removeThisView(node) {
    node = escape(node.id);
    console.log(node);
    //$("#"+node.id).remove();

}
RegExp.escape = function (text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};
$(function () {
    $("#displayDeadline").datepicker({
        dateFormat: "dd-mm-yy"
    }).datepicker("setDate", new Date());
    $("#displayDeadline").click(function () {
        $("#displayDeadline").datepicker("show");
    });
});
$(function () {
    $("#duedate").datepicker({
        dateFormat: "dd-mm-yy"
    }).datepicker("setDate", new Date());

});
function routeTo(route) {
    console.log(route);
    switch (route) {
        case WELCOME:
        {
            rootRef.unauth();
            screen(WELCOME);
        }
            break;
        case MAIN:
        {
            screen(MAIN);
        }
            break;
        case PROJECT:
        {
            screen(PROJECT);
        }
            break;
    }
    window.location.href = '#' + route;
}

$("#action").click(function (e) {
    var $currentButton = $(this);
    var provider = $currentButton.data('provider');
    var login = $('#login_fragment');
    if ($('a#action').text() === "Logout") {
        routeTo(WELCOME);
        return;
    }
    if (login.is(':visible')) {
        $('a#action').text('Login');
        login.hide('slide', {direction: 'left'}, 400);
        reg.show();
    }
    else {
        $('a#action').text('Sign Up');
        reg.hide('slide', {direction: 'left'}, 400);
        login.show();
    }
});
function getScope(ctrlName) {
    var sel = 'div[ng-controller="' + ctrlName + '"]';
    return angular.element(sel).scope();
}
function thirdPartyLogin(provider) {
    var deferred = $.Deferred();
    rootRef.authWithOAuthPopup(provider, function (err, user) {
        if (err) {
            deferred.reject(err);
        }
        if (user) {
            deferred.resolve(user);
        }
    }, {
        scope: "email" // the permissions requested
    });
    return deferred.promise();
}

function authWithPassword(userObj) {
    var deferred = $.Deferred();
    console.log(userObj);
    rootRef.authWithPassword(userObj, function onAuth(err, user) {
        if (err) {
            deferred.reject(err);
        }
        if (user) {
            deferred.resolve(user);
        }
    });
    return deferred.promise();
}

function createUser(userObj) {
    var deferred = $.Deferred();
    rootRef.createUser(userObj, function (err) {

        if (!err) {
            deferred.resolve();
        } else {
            deferred.reject(err);
        }

    });
    return deferred.promise();
}

function createUserAndLogin(userObj) {
    return createUser(userObj)
        .then(function () {
            return authWithPassword(userObj);
        });
}

function handleAuthResponse(promise, provider, data) {
    console.log(promise);
    $.when(promise)
        .then(function (authData) {
            console.log("data " + JSON.stringify(authData));
            userName = provider === 'email' ? data.nama : "Welcome " + authData.facebook.displayName;
            routeTo(MAIN);
        }, function (err) {
            console.log(err);
        });
}


function screen(type) {
    console.log(type);
    switch (type) {
        case WELCOME:
        {
            $('#signup_fragment').hide();
            $("#welcome").show().fadeIn(750);
            $("#main").hide().fadeOut(750);
            $('a#action').text('Sign Up');
            $('a#profile').text("Welcome");
            $("#project-screen").hide().fadeOut(750);
        }
            break;
        case MAIN:
        {
            $("#welcome").hide().fadeOut(750);
            $("#main").show().fadeIn(750);
            $('a#action').text('Logout');
            $('a#profile').text(userName);
            $("#project-screen").hide().fadeOut(750);
        }
            break;
        case PROJECT:
        {
            $("#welcome").hide().fadeOut(750);
            $("#main").hide().fadeOut(750);
            $("#project-screen").show().fadeIn(750);

        }
            break;
    }
}
app.factory("Project", ["$firebaseObject",
    function ($firebaseObject) {
        return function (projectname) {
            var profileRef = rootRef.child('projects').child(projectname);
            return $firebaseObject(profileRef);
        }
    }
]);

app.controller('LoginController', ['$scope', function ($scope) {
    $scope.loginFB = function (e) {
        handleAuthResponse(thirdPartyLogin("facebook"), 'profile', e);
        first = false;
    };
    $scope.login = function (user) {
        console.log(user);
        handleAuthResponse(authWithPassword(user), 'profile');
        first = false;
    };
    $scope.register = function (data) {
        console.log(data);
        var loginPromise = createUserAndLogin(data);
        handleAuthResponse(loginPromise, 'email', data);
    };
}])
    .controller('ProjectController', ["$scope", "Project",
        function ($scope, Project) {
            $scope.projects = [];
            $scope.whom = [];
            $scope.checklists = [];
            $scope.subtask = [];
            var id;
            $scope.$watch(function () {
                addDragable();
            });
            $scope.addSubtask = function (event, data) {
                $scope.subtask = [];
                console.log($scope.checklists.length);
                if ($scope.checklists.length == 0) {
                    $scope.checklists.push({
                        name: null, done: true, ide: null
                    })
                }
                var res = event.target.id.split("/");
                $scope.subtask.push({
                    name: data.name,
                    status: 0,
                    desc: data.description,
                    checklist: $scope.checklists
                });
                var newData = $scope.subtask[0];
                console.log(newData);
                task(id, res[0]).child('whom').child(res[1]).child('subtasks').push(angular.fromJson(angular.toJson(newData)), function callback(respon) {
                });

            };
            var ide = 0;
            $scope.currentOpenedSubtask = function (id) {
                var res = id.target.id.split("/");
                $scope.cuid = res[0];
                $scope.ctid = res[1];
                $scope.cstid = res[2];
                $('#modalEditSubTask').openModal();
                console.log(res)
            };
            $scope.saveTaskName = function (ide, model) {
                if ($("#" + ide.target.id).val() === '') {
                    return
                }
                var idi = ide.target.id;
                console.log(ide.target.id + model);
                task(id, idi.substring(0, idi.length - 1)).update(model);
                $("#" + ide.target.id).val('');
            };
            $scope.addChecklist = function () {
                $scope.checklists.push({
                    name: $scope.checklist, done: false, ide: ide
                });
                ide++;
                $scope.checklist = '';
                console.log($scope.subtask);
            };
            $scope.deleteChecklist = function (checklist) {
                $scope.project.task[$scope.ctid].whom[$scope.cuid].subtasks[$scope.cstid].checklist[checklist].valueModel('');
                console.log(checklist);
            };
            $scope.addEChecklist = function (data) {
                console.log($scope.project.task[$scope.ctid].whom[$scope.cuid].subtasks[$scope.cstid].checklist);
                $scope.project.task[$scope.ctid].whom[$scope.cuid].subtasks[$scope.cstid].checklist.push({
                    name: data, done: false, ide: ide
                });
                ide++;
                $scope.checklist = '';

            };
            $scope.submitTask = function (task) {
                var detail = {name: task.nama, whom: {}, desc: task.deskripsi, deadline: task.tanggal};
                angular.forEach($scope.whom, function (whom) {
                    detail.whom[whom.id] = whom.yes;
                });
                var aTask = tasks(id).push(detail, function (callback) {
                    console.log(aTask.key());
                });
            };
            $scope.loadMember = function () {
                $scope.whom = [];
                var i = 0;
                var dataa = [];
                for (data in $scope.project.member) {
                    dataa.push(data);
                }
                for (data in $scope.project.member) {
                    rootRef.child('users').child(data).child('displayName').once("value", function (snapshot) {
                        $scope.$apply(function () {
                            var val = snapshot.val();
                            $scope.whom.push({
                                id: dataa[i],
                                name: val,
                                yes: true
                            });
                            i++;
                        });
                    });
                }
            };
            $scope.seeThis = function (projectID) {
                NProgress.configure({showSpinner: false});
                NProgress.start();
                project(projectID).once("value", function (snapshot) {
                    id = projectID;
                    $scope.project = [];
                    Project(projectID).$bindTo($scope, "project").then(function () {
                        rootRef.child('projects').child(projectID).child('member').on("value", function (snapshot) {
                            $scope.loadMember();
                            NProgress.done();
                        });
                        routeTo(PROJECT);
                    });
                });
            };
            $scope.muat = function (uid) {
                NProgress.configure({showSpinner: false});
                NProgress.start();

                $scope.projects = [];
                rootRef.child('users').child(uid).child('projects').once('value', function (snapshot) {
                    snapshot.forEach(function (msg) {
                        var path = msg.key();
                        project(path).once("value", function (snapshot) {
                            $scope.$apply(function () {
                                var val = snapshot.val();
                                $scope.projects.push({
                                    desc: val.desc,
                                    name: val.name,
                                    id: path
                                });
                            });
                        });
                    });
                    NProgress.done();
                });

            };
            $scope.add = function (proyek) {
                var myObject = {name: proyek.nama, userID: uid, member: {}, desc: proyek.deskripsi, public: false};
                myObject.member[uid] = true;
                console.log(myObject);
                var projectAdd = projects.push(myObject, function onComplete() {
                    console.log(projectAdd.key());
                    var jsonVariable = {};
                    var jsonKey = projectAdd.key();
                    jsonVariable[jsonKey] = true;
                    project(jsonKey).on("value", function (snapshot) {
                        $scope.$apply(function () {
                            var val = snapshot.val();
                            $scope.projects.push({
                                desc: val.desc,
                                name: val.name,
                                id: jsonKey
                            });
                            console.log($scope.projects);
                        });
                    });
                    rootRef.child('users').child(uid).child('projects').update(jsonVariable, function (error) {
                        if (error) {
                        } else {

                        }
                    });
                });

            };
            function tilt_direction(item) {
                var left_pos = item.position().left,
                    move_handler = function (e) {
                        if (e.pageX >= left_pos) {
                            item.addClass("right");
                            item.removeClass("left");
                        } else {
                            item.addClass("left");
                            item.removeClass("right");
                        }
                        left_pos = e.pageX;
                    };
                $("html").bind("mousemove", move_handler);
                item.data("move_handler", move_handler);
            }

            function addDragable() {
                var taskID;
                var userID;
                var subtaskID;
                $(".portlet").sortable({
                    items: '.card-panel',
                    placeholder: "portlet-placeholder",
                    connectWith: '.portlet',
                    dropOnEmpty: true,
                    receive: function (e, ui) {
                        $(this).append(ui.item);
                        var newUserID = ui.item.parent().attr('id');
                        var newTaskID = ui.item.parent().closest('tr').attr('id');
                        var ftask = $scope.project.task[taskID].whom[userID].subtasks[subtaskID];
                        ui.item.remove();
                        task(id, newTaskID).child('whom').child(newUserID).child('subtasks').push(ftask, function onComplete() {

                            task(id, taskID).child('whom').child(userID).child('subtasks').child(subtaskID).remove();

                        });
                    },
                    start: function (event, ui) {
                        ui.placeholder.height(ui.item.height());
                        ui.item.addClass('tilt');
                        console.log(ui.item.attr('id'));
                        tilt_direction(ui.item);
                        var aidi = ui.item.attr('id').split("/");
                        taskID = aidi[1];
                        userID = aidi[0];
                        subtaskID = aidi[2];

                    },
                    stop: function (event, ui) {
                        ui.item.removeClass("tilt");
                        $("html").unbind('mousemove', ui.item.data("move_handler"));
                        ui.item.removeData("move_handler");


                    }
                });
            }
        }]);
(function (jQuery, Firebase, Path) {
    "use strict";
    $('.modal-trigger').leanModal(
        $(function () {

            Path.listen();
            rootRef.onAuth(function globalOnAuth(authData) {
                if (authData) {
                    console.log(authData.provider);
                    if(authData.provider==='facebook'){
                        userName = "Welcome " + authData.facebook.displayName;
                        email=authData.facebook.email;
                    }else{
                        userName = "Welcome " + authData.nama;
                        email=authData.email;
                    }

                    uid = authData.uid;
                    projects.on("child_changed", function (snapshot) {
                        console.log(snapshot.val())
                    });
                    rootRef.child('users').once('value', function (snapshot) {
                        if (!snapshot.hasChild(authData.uid)) {
                            rootRef.child('users').child(authData.uid).set({
                                displayName: userName,
                                email: email
                            }, function onComplete() {

                            });

                        }

                    });

                    routeTo(MAIN);
                    var $scope = getScope('ProjectController');
                    $scope.muat(authData.uid);
                    $scope.$apply();
                    if (!first) {
                        toastr.success("Log in")
                    }

                } else {
                    routeTo(WELCOME);
                    if (!first) {
                        toastr.warning("Log out")
                    }
                }
            });
        }));
}(window.jQuery, window.Firebase, window.Path));
